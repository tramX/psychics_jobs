from django.apps import AppConfig


class AppPsychicsJobsConfig(AppConfig):
    name = 'app_psychics_jobs'
