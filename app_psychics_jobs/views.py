from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, UpdateView, View
from django.contrib.auth.mixins import AccessMixin
from django.contrib.auth import authenticate, login
from django.utils.crypto import get_random_string

import random

from app_psychics_jobs import models


class LoginRequiredMixin(AccessMixin):
    """Verify that the current user is authenticated."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            username = get_random_string()
            password = get_random_string()
            models.User.objects.create_user(username, password=password, is_staff=True)
            user = authenticate(username=username, password=password)
            login(self.request, user)

        return super().dispatch(request, *args, **kwargs)


class BaseContent(View):
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['psychics_profiles'] = models.PsychicsProfile.objects.all()
        return ctx


class IndexPage(BaseContent, LoginRequiredMixin, CreateView):
    success_url = '/'
    template_name = 'index.html'
    model = models.Question
    fields = ()

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        if models.PsychicsProfile.objects.count() == 0:
            user = models.User.objects.create_user('psychic1', password='1qazxsw2')
            models.PsychicsProfile.objects.create(user=user)

            user = models.User.objects.create_user('psychic2', password='1qazxsw2')
            models.PsychicsProfile.objects.create(user=user)

            user = models.User.objects.create_user('psychic3', password='1qazxsw2')
            models.PsychicsProfile.objects.create(user=user)

        for psychic in models.PsychicsProfile.objects.all():
            models.Guessing.objects.create(user=psychic.user, question=self.object,
                                           number=random.choice(range(10, 100)))

        self.success_url = reverse('question-update', kwargs={'pk': self.object.id})

        return super().form_valid(form)


class LogintPage(TemplateView):
    template_name = 'login.html'

    def post(self, request, *args, **kwargs):
        user = authenticate(username=self.request.POST.get('username'), password=self.request.POST.get('password'))

        if user is not None:
            if user.is_active:
                login(request, user)

        return redirect(reverse('index-page'))


class QuestionUpdateView(BaseContent, LoginRequiredMixin, UpdateView):
    model = models.Question
    template_name = 'index.html'
    success_url = '/'
    fields = ('number',)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()

        for guessing in models.Guessing.objects.filter(question=self.object):
            if guessing.number == self.object.number:
                guessing.user.user_psychics_profile.accuracy += 1
            else:
                if (guessing.user.user_psychics_profile.accuracy - 1) >= 0:
                    guessing.user.user_psychics_profile.accuracy -= 1

            guessing.user.user_psychics_profile.save()

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['guessing_list'] = models.Guessing.objects.filter(question=self.object)
        return ctx
