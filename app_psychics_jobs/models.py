from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

from django.db import models


class PsychicsProfile(models.Model):
    user = models.OneToOneField(User, related_name='user_psychics_profile', on_delete=models.CASCADE)
    accuracy = models.PositiveIntegerField(default=0)


class Question(models.Model):
    user = models.ForeignKey(User, related_name='user_questions', on_delete=models.CASCADE)
    number = models.PositiveIntegerField(validators=[MinValueValidator(10), MaxValueValidator(99)],
                                         blank=True, null=True, verbose_name='Введите загаданное число')


class Guessing(models.Model):
    user = models.ForeignKey(User, related_name='user_guessings', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name='question_guessing', on_delete=models.CASCADE)
    number = models.PositiveIntegerField(validators=[MinValueValidator(10), MaxValueValidator(99)],
                                         blank=True, null=True)
