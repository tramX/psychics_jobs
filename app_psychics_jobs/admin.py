from django.contrib import admin

from app_psychics_jobs import models

admin.site.register(models.Question)
admin.site.register(models.Guessing)
admin.site.register(models.PsychicsProfile)
